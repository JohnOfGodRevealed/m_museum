import 'package:flutter/material.dart';
import 'package:m_museum/Models/Museum.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:m_museum/Bloc/MuseumBloc.dart';
import 'package:m_museum/Bloc/CountryBloc.dart';
import 'package:m_museum/Models/Country.dart';
import 'package:m_museum/main.dart';

class MuseumScreen extends StatefulWidget {
  Museum? musee;
  MuseumScreen({ Key? key , this.musee}) : super(key: key);

  @override
  State<MuseumScreen> createState() => _MuseumScreenState();
}

class _MuseumScreenState extends State<MuseumScreen> {
  TextEditingController txtCodePays = TextEditingController();
  TextEditingController txtNomMus = TextEditingController();
  TextEditingController txtNblivres = TextEditingController();
  bool validate_nom = true;
  bool validate_nblivres = true;
  String saveOrUpdateText = '';
  String codePays = '';
  final MuseumBloc museeBloc = MuseumBloc();
  final CountryBloc paysBloc = CountryBloc();
  List<Country> listPays = [];

  @override
  void initState() {
    
    if (widget.musee != null){
      txtCodePays.text = widget.musee!.codePays.toString();
      txtNomMus.text = widget.musee!.nomMus.toString();
      txtNblivres.text = widget.musee!.nblivres.toString();
      saveOrUpdateText = "Modifier";
    }

     paysBloc.getCountries().then((value){
      listPays = value;
      codePays = listPays[0].codePays;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(backgroundColor: myColor, title: Text("Musée"),),
      body: Center(
        child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 300,
                      height: 40,
                      child:Row(
                        children: [
                          const Text("Pays"),
                          const Spacer(),
                          DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: false,
                              items: listPays.map((pays) {
                                return DropdownMenuItem<String>(
                                  value: pays.codePays,
                                  child: SizedBox(
                                    width: 150, //expand here
                                    child: Text(
                                      pays.codePays,
                                      style: const TextStyle(fontSize: 15),
                                      textAlign: TextAlign.end,
                                    ),
                                  ),
                                );
                              }).toList(),
                              onChanged: (newValue) {
                                setState(() {
                                  codePays = newValue.toString();
                                });
                              },
                              hint: const SizedBox(
                                width: 150, //and here
                                child: Text(
                                  "Code du pays",
                                  style: TextStyle(color: Colors.grey),
                                  textAlign: TextAlign.end,
                                ),
                              ),
                              style: TextStyle(color: myColor, decorationColor: Colors.red),
                              value: codePays,
                            ),
                          ),
                        ],
                      ),
                            
                    ),
                    const SizedBox(height: 10,),
                    SizedBox(
                      width: 300,
                      height: 40,
                      child:TextFormField(
                        controller: txtNomMus,
                        style: const TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(fontSize: 14,),
                          hintText: 'Nom du musée',
                          errorStyle: const TextStyle(color: Color(0xFFFDA384)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: myColor),
                          ),
                          errorText: validate_nom == false ? 'Le champs est obligatoire ' : null,
                        ),
                        cursorColor: myColor,
                      ),                    
                    ),
                    const SizedBox(height: 10,),
                    SizedBox(
                      width: 300,
                      height: 40,
                      child:
                      TextFormField(
                        controller: txtNblivres,
                        keyboardType: TextInputType.number,
                        style: const TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                          hintStyle: const TextStyle(fontSize: 14,),
                          hintText: "Nombre d'habitants",
                          errorText: validate_nblivres == false ? 'Le champs est obligatoire ' : null,
                          errorStyle: const TextStyle(color: Color(0xFFFDA384)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: myColor),
                          ), 
                        ),
                        cursorColor: myColor,
                      ), 
                    ),
                    const SizedBox(height: 20,),
                    SizedBox(
                      width: 300,
                      height: 40,
                      child: ElevatedButton(
                          onPressed: () async {
                            save();
                            Navigator.pop(context);
                          },
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all<Color>(myColor),
                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                                //side: BorderSide(color: Colors.red)
                              ))),
                          //color: const Color(0xFF390047),
                          child: Text(
                            saveOrUpdateText,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 13.0,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                        ),
                    ),
                    const SizedBox(height: 20,),
                   ],
                ),
      ),
    );
          
  }

   @override
  void save() {
    setState(() {
      txtNomMus.text.trim().isEmpty
          ? validate_nom = false
          : validate_nom = true;
      txtNblivres.text.trim().isEmpty
          ? validate_nblivres = false
          : validate_nblivres = true;
    });
    if (validate_nom && validate_nblivres) {
      Museum musee = Museum(
        numMus: 0,
        nomMus: txtNomMus.text.trim(), 
        nblivres:  int.parse(txtNblivres.text.trim()),
        codePays: codePays,
      );
      if (saveOrUpdateText == 'Enregistrer'){
        museeBloc.addMuseum(musee);
      }else{
        museeBloc.updateMuseum(musee);
      }
     
    }
  }

  @override
  void delete(Museum musee) {
    var data = museeBloc.getMuseumFromOtherTables(musee.numMus).then((value) {
      print('value $value');
      if (value == false){
        museeBloc.deleteMuseum(musee);
      }else{
        Fluttertoast.showToast(
          msg: "Désolé, vous ne pouvez pas supprimer ce musée car il est utilisé pour d'autres enregistrement",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 2,
          backgroundColor: Colors.blue,
          textColor: Colors.white,
          fontSize: 13.0
        );
      }
    });
  }
}