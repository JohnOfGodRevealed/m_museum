import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:m_museum/Screens/LibraryIndex.dart';
import 'package:m_museum/Screens/MuseumIndex.dart';
import 'package:m_museum/Screens/CountryIndex.dart';
import 'package:m_museum/Screens/VisitIndex.dart';
import 'package:m_museum/main.dart';
import 'package:m_museum/Database/DatabaseProvider.dart';
import 'package:m_museum/Screens/BookIndex.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var currentPage = DrawerSections.listePays;

  @override
  void initState() {
    DatabaseProvider museeDatabase = DatabaseProvider.databaseProvider;
    super.initState();
  }

  toggleDrawer() async {
    if (_scaffoldKey.currentState!.isDrawerOpen) {
      _scaffoldKey.currentState!.openEndDrawer();
    } else {
      _scaffoldKey.currentState!.openDrawer();
    }
  }

  @override
  Widget build(BuildContext context) {
    var container;
    if (currentPage == DrawerSections.listePays) {
      container = const CountryIndex();
    } else if (currentPage == DrawerSections.listeMusees) {
      container = const MuseumIndex();
    } else if (currentPage == DrawerSections.listeOuvrages) {
      container = const BookIndex();
    } else if (currentPage == DrawerSections.listeBibliotheques) {
      container = const LibraryIndex();
    } else if (currentPage == DrawerSections.listeVisites) {
      container = const VisitIndex();
    }

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Mon super musée'),
          backgroundColor: myColor,
          leading: Builder(
            builder: (context) => // Ensure Scaffold is in context
                IconButton(
                    icon: const Icon(
                      Icons.menu,
                    ),
                    onPressed: () {
                      Scaffold.of(context).openDrawer();
                    }),
          ),
        ),
        drawer: Drawer(
          child: Container(
            padding: const EdgeInsets.only(top: 0, left: 0),
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    // Important: Remove any padding from the ListView.
                    padding: const EdgeInsets.all(0),
                    dragStartBehavior: DragStartBehavior.start,
                    children: [
                       DrawerHeader(
                        child:  Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: const [
                            Text(
                              '🏛', textAlign: TextAlign.left,
                              style: TextStyle(color: Colors.black, fontSize: 82, ),
                            ),
                            Text(
                              'Mon Super Musée ',
                              style: TextStyle(color: Colors.black, fontSize: 22),
                            ),
                          ],
                        ),


                        decoration: BoxDecoration(
                            color: Colors.green,
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image:
                                    AssetImage('assets/images/pattern.jpg'))),
                      ),
                      MyDrawerList(),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                      alignment: FractionalOffset.centerLeft,
                      child: Column(
                        children: const [
                          Divider(),
                          Text('Fait avec ❤️ et EA.')
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
        body: container,
      ),
    );
  }

  Widget MyDrawerList() {
    return Container(
      padding: const EdgeInsets.only(
        top: 15,
      ),
      child: Column(
        // shows the list of menu drawer
        children: [
          menuItem(1, "Pays", Icons.flag,
              currentPage == DrawerSections.listePays ? true : false),
          Divider(),
          menuItem(2, "Musées", Icons.museum,
              currentPage == DrawerSections.listeMusees ? true : false),
          Divider(),
          menuItem(3, "Ouvrages", Icons.book,
              currentPage == DrawerSections.listeOuvrages ? true : false),
          Divider(),
          menuItem(4, "Bibliothèques", Icons.local_library,
              currentPage == DrawerSections.listeBibliotheques ? true : false),
          Divider(),
          menuItem(5, "Visites", Icons.people,
              currentPage == DrawerSections.listeVisites ? true : false),
        ],
      ),
    );
  }

  Widget menuItem(int id, String title, icon, bool selected) {
    return Material(
      child: InkWell(
        onTap: () {
          toggleDrawer();
          setState(() {
            if (id == 1) {
              currentPage = DrawerSections.listePays;
            } else if (id == 2) {
              currentPage = DrawerSections.listeMusees;
            } else if (id == 3) {
              currentPage = DrawerSections.listeOuvrages;
            } else if (id == 4) {
              currentPage = DrawerSections.listeBibliotheques;
            } else if (id == 5) {
              currentPage = DrawerSections.listeVisites;
            }
          });
        },
        child: Padding(
          padding: const EdgeInsets.only(left: 20, top: 15),
          child: Row(
            children: [
              Icon(icon, color: const Color(0xF1003E80)),
              Expanded(
                child: Text(
                  '   $title',
                  style: const TextStyle(
                    color: Colors.black,
                    letterSpacing: 2,
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

enum DrawerSections {
  listePays,
  listeBibliotheques,
  listeOuvrages,
  listeMusees,
  listeVisites,
}
