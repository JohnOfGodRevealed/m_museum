
import 'dart:async';

import 'package:m_museum/Models/Library.dart';
import 'package:m_museum/Repository/LibraryRepository.dart';

class LibraryBloc {
  final _libraryRepository = LibraryRepository();
  final _libraryController = StreamController<List>.broadcast();
  get libraries => _libraryController.stream;

  LibraryBloc() {
    getLibraries();
  }

  getLibraries() async {
    _libraryController.sink.add(await _libraryRepository.index());
  }

  create(Library library) async {
    await _libraryRepository.create(library);
    getLibraries();
  }

  update(Library library) async {
    await _libraryRepository.update(library);
    getLibraries();
  }

  destroy(String isbn, int numMus) async {
    _libraryRepository.destroy(isbn, numMus);
    getLibraries();
  }

  dispose() {
    _libraryController.close();
  }
}