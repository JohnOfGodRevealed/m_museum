import 'dart:async';

import 'package:m_museum/Models/Country.dart';
import 'package:m_museum/Repository/CountriesRepository.dart';

class CountryBloc {
  final _countriesRepository = CountriesRepository();
  final _countryController = StreamController<List<Country>>.broadcast();
  get countries => _countryController.stream;

  CountryBloc() {
    getCountries();
  }

  Future<List<Country>> getCountries() async {
    _countryController.sink.add(await _countriesRepository.index());
    return _countriesRepository.index();
  }

  addCountry(Country country) async {
    await _countriesRepository.create(country);
    getCountries();
  }

  updateCountry(Country country) async {
    await _countriesRepository.update(country);
    getCountries();
  }

  deleteCountry(Country country) async {
    _countriesRepository.destroy(country);
    getCountries();
  }

  Future<bool> getCountryFromOtherTables(String code) async {
    return _countriesRepository.getFromOtherTables(code);
  }

  dispose() {
    _countryController.close();
  }
}