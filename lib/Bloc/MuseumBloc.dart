
import 'dart:async';

import 'package:m_museum/Models/Museum.dart';
import 'package:m_museum/Repository/MuseumRepository.dart';

class MuseumBloc {
  final _museumRepository = MuseumRepository();
  final _museumController = StreamController<List<Museum>>.broadcast();
  get museums => _museumController.stream;

  MuseumBloc() {
    getMuseums();
  }

  Future<List<Museum>> getMuseums() async {
    _museumController.sink.add(await _museumRepository.index());
    return _museumRepository.index();
  }

  addMuseum(Museum museum) async {
    await _museumRepository.create(museum);
    getMuseums();
  }

  updateMuseum(Museum museum) async {
    await _museumRepository.update(museum);
    getMuseums();
  }

  deleteMuseum(Museum museum) async {
    _museumRepository.destroy(museum);
    getMuseums();
  }

  Future<bool> getMuseumFromOtherTables(int numMus) async {
    return _museumRepository.getFromOtherTables(numMus);
  }

  dispose() {
    _museumController.close();
  }
}