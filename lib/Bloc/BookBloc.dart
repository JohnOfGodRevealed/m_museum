import 'dart:async';

import 'package:m_museum/Models/Book.dart';
import 'package:m_museum/Repository/BookRepository.dart';

class BookBloc {
  final _bookRepository = BookRepository();
  final _bookController = StreamController<List<Book>>.broadcast();
  get books => _bookController.stream;

  BookBloc() {
    getBooks();
  }

  Future<List<Book>> getBooks() async {
    _bookController.sink.add(await _bookRepository.index());
    return _bookRepository.index();
  }

  addBook(Book book) async {
    await _bookRepository.create(book);
    getBooks();
  }

  updateBook(Book book) async {
    await _bookRepository.update(book);
    getBooks();
  }

  deleteBook(Book book) async {
    _bookRepository.destroy(book);
    getBooks();
  }

  Future<bool> getBookFromOtherTables(String code) async {
    return _bookRepository.getFromOtherTables(code);
  }

  dispose() {
    _bookController.close();
  }
}
