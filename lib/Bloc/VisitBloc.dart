
import 'dart:async';

import 'package:m_museum/Models/Visit.dart';
import 'package:m_museum/Repository/VisitRepository.dart';

class VisitBloc {
  final _visitRepository = VisitRepository();
  final _visitController = StreamController<List>.broadcast();
  get visits => _visitController.stream;

  VisitBloc() {
    getVisits();
  }

  getVisits() async {
    _visitController.sink.add(await _visitRepository.index());
  }

  addVisit(Visit visit) async {
    await _visitRepository.create(visit);
    getVisits();
  }

  updateVisit(Visit visit) async {
    await _visitRepository.update(visit);
    getVisits();
  }

  deleteVisit(String jour, int numMus) async {
    _visitRepository.destroy(jour, numMus);
    getVisits();
  }

  dispose() {
    _visitController.close();
  }
}