
import 'package:m_museum/Models/Visit.dart';

import '../Database/DatabaseProvider.dart';

class VisitDao{
  final databaseProvider = DatabaseProvider.databaseProvider;

  Future close() async{
    final db = await databaseProvider.database;
    db.close();
  }

  Future<List> index() async{
    final db = await databaseProvider.database;
    List result = await db.rawQuery(
        'SELECT VISITER.NumMus, MUSEE.NomMus, MOMENT.jour, VISITER.nbvisiteurs FROM VISITER, MUSEE, MOMENT WHERE VISITER.NumMus = MUSEE.NumMus AND MOMENT.jour = VISITER.jour');
    return result;
  }

  Future<int> create(Visit visit) async {
    final db = await databaseProvider.database;
    return await db.insert("VISITER", visit.toJson());
  }

  Future<int> update(Visit visit) async{
    final db = await databaseProvider.database;
    return await db.update(
      "VISITER", visit.toJson(),
      where: 'jour = ? AND numMus = ?', 
      whereArgs: [visit.jour, visit.numMus]);
  }

  Future<int> destroy(String jour, int numMus) async {
    final db = await databaseProvider.database;
    return await db.delete(
      "VISITER", 
      where: 'jour = ? AND numMus = ?', 
      whereArgs: [jour, numMus]);
  }

}