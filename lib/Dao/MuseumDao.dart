import 'package:m_museum/Models/Museum.dart';

import '../Database/DatabaseProvider.dart';

class MuseumDao{
  final databaseProvider = DatabaseProvider.databaseProvider;

  Future close() async{
    final db = await databaseProvider.database;
    db.close();
  }
  Future<List<Museum>> index() async{
    final db = await databaseProvider.database;
    List result = await db.query("MUSEE", orderBy: 'numMus ASC');
    return result.map((json)=>Museum.fromJson(json)).toList();
  }

  Future<int> create(Museum museum) async {
    final db = await databaseProvider.database;
    return await db.insert("MUSEE", museum.toJson());
  }

  Future<int> update(Museum museum) async{
    final db = await databaseProvider.database;
    return await db.update(
      "MUSEE", museum.toJson(),
      where: 'numMus = ?', 
      whereArgs: [museum.numMus]);
  }

  Future<int> destroy(Museum museum) async {
    final db = await databaseProvider.database;
    return await db.delete(
      "MUSEE", 
      where: 'numMus = ?', 
      whereArgs: [museum.numMus]);
  }

  Future<bool> getMuseumFromOtherTables(int numMus) async{
    final db = await databaseProvider.database;
    var maps = await db.query("BIBLIOTHEQUE",
      where: 'NumMus = ?', 
      whereArgs: [numMus]);

    if(maps.isNotEmpty){
      return true;
    }else{
      maps = await db.query("VISITER",
      where: 'NumMus = ?', 
      whereArgs: [numMus]);

      if(maps.isNotEmpty){
        return true;
      }else{
        print("Ce musée n'a pas été utilisé dans une autre table");
        return false;
      }
    }
  }
}