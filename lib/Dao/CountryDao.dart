import 'package:m_museum/Database/DatabaseProvider.dart';
import 'package:m_museum/Models/Country.dart';

class CountryDao {
  final databaseProvider = DatabaseProvider.databaseProvider;

  Future close() async {
    final db = await databaseProvider.database;
    db.close();
  }

  Future<List<Country>> index() async {
    final db = await databaseProvider.database;
    List result = await db.query("PAYS", orderBy: 'codePays ASC');
    return result.map((json) => Country.fromJson(json)).toList();
  }

  Future<int> create(Country country) async {
    final db = await databaseProvider.database;
    return await db.insert("PAYS", country.toJson());
  }

  Future<Country> getByCode(String code) async {
    final db = await databaseProvider.database;
    final maps =
        await db.query("PAYS", where: 'codePays = ?', whereArgs: [code]);
    if (maps.isNotEmpty) {
      return Country.fromJson(maps.first);
    } else {
      print('Not found');
      return Country.fromJson(maps.first);
    }
  }

  Future<int> update(Country country) async {
    final db = await databaseProvider.database;
    return await db.update("PAYS", country.toJson(),
        where: 'codePays = ?', whereArgs: [country.codePays]);
  }

  Future<int> destroy(Country country) async {
    final db = await databaseProvider.database;
    return await db.delete("PAYS",
        where: 'codePays = ?', whereArgs: [country.codePays]);
  }

  Future<bool> getCountryFromOtherTables(String code) async {
    final db = await databaseProvider.database;
    var maps =
        await db.query("MUSEE", where: 'codePays = ?', whereArgs: [code]);

    Country country = Country(codePays: '', nbhabitant: 0);
    if (maps.isNotEmpty) {
      country = Country.fromJson(maps.first);
      return true;
    } else {
      maps = await db
          .query("OUVRAGE", where: 'codePays = ?', whereArgs: [code]);

      if (maps.isNotEmpty) {
        country = Country.fromJson(maps.first);
        return true;
      } else {
        print("Ce pays n'a pas été utilisé dans une autre table");
        return false;
      }
    }
  }
}
