
import 'package:m_museum/Models/Book.dart';
import '../Database/DatabaseProvider.dart';

class BookDao{
  final databaseProvider = DatabaseProvider.databaseProvider;

  Future close() async{
    final db = await databaseProvider.database;
    db.close();
  }

  Future<List<Book>> index() async{
    final db = await databaseProvider.database;
    List result = await db.query("OUVRAGE", orderBy: 'isbn ASC');
    return result.map((json)=>Book.fromJson(json)).toList();
  }

  Future<int> create(Book book) async {
    final db = await databaseProvider.database;
    return await db.insert("OUVRAGE", book.toJson());
  }

  Future<int> update(Book book) async{
    final db = await databaseProvider.database;
    return await db.update(
      "OUVRAGE", book.toJson(),
      where: 'isbn = ?', 
      whereArgs: [book.isbn]);
    
  }

  Future<int> destroy(Book book) async {
    final db = await databaseProvider.database;
    return await db.delete(
      "OUVRAGE", 
      where: 'isbn = ?', 
      whereArgs: [book.isbn]);
  }

  Future<bool> getBookFromOtherTables(String ISBN) async{
    final db = await databaseProvider.database;
    var maps = await db.query("BIBLIOTHEQUE",
      where: 'ISBN = ?', 
      whereArgs: [ISBN]);

    if(maps.isNotEmpty){
      return true;
    }else{
      print("Cet book n'a pas été utilisé dans une autre table");
      return false;
    }
  }

}