
import 'package:m_museum/Models/Library.dart';

import '../Database/DatabaseProvider.dart';

class LibraryDao{
  final databaseProvider = DatabaseProvider.databaseProvider;

  Future close() async{
    final db = await databaseProvider.database;
    db.close();
  }

  Future<List> index() async{
    final db = await databaseProvider.database;
    List result = await db.rawQuery(
        'SELECT BIBLIOTHEQUE.NumMus, MUSEE.NomMus, BIBLIOTHEQUE.isbn, OUVRAGE.titre, dateAchat FROM BIBLIOTHEQUE, OUVRAGE, MUSEE WHERE BIBLIOTHEQUE.isbn = OUVRAGE.isbn AND BIBLIOTHEQUE.NumMus = MUSEE.NumMus');
    return result;
  }


  Future<int> create(Library library) async {
    final db = await databaseProvider.database;
    return await db.insert("BIBLIOTHEQUE", library.toJson());
  }

  Future<int> update(Library library) async{
    final db = await databaseProvider.database;

    return await db.update(
      "BIBLIOTHEQUE", library.toJson(),
      where: 'isbn = ? AND numMus = ?', 
      whereArgs: [library.isbn, library.numMus]);
    
  }

  Future<int> destroy(String isbn, int numMus) async {
    final db = await databaseProvider.database;
    return await db.delete(
      "BIBLIOTHEQUE", 
      where: 'isbn = ? AND numMus = ?', 
      whereArgs: [isbn, numMus]);
  }
}