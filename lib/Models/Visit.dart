// To parse this JSON data, do
//
//     final visiter = visiterFromJson(jsonString);

import 'dart:convert';

Visit visitFromJson(String str) => Visit.fromJson(json.decode(str));

String visitToJson(Visit data) => json.encode(data.toJson());

class Visit {
    Visit({
        required this.numMus,
        required this.jour,
        required this.nbvisiteurs,
    });

    int numMus;
    String jour;
    int nbvisiteurs;

    factory Visit.fromJson(Map<String, dynamic> json) => Visit(
        numMus: json["numMus"],
        jour: json["jour"],
        nbvisiteurs: json["nbvisiteurs"],
    );

    Map<String, dynamic> toJson() => {
        "numMus": numMus,
        "jour": jour,
        "nbvisiteurs": nbvisiteurs,
    };
}
