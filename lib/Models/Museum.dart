// To parse this JSON data, do
//
//     final musee = museeFromJson(jsonString);

import 'dart:convert';

Museum museumFromJson(String str) => Museum.fromJson(json.decode(str));

String museumToJson(Museum data) => json.encode(data.toJson());

class Museum {
    Museum({
        required this.numMus,
        required this.nomMus,
        required this.nblivres,
        required this.codePays,
    });

    int numMus;
    String nomMus;
    int nblivres;
    String codePays;

    factory Museum.fromJson(Map<String, dynamic> json) => Museum(
        numMus: json["numMus"],
        nomMus: json["nomMus"],
        nblivres: json["nblivres"],
        codePays: json["codePays"],
    );

    Map<String, dynamic> toJson() => {
        // "numMus": numMus,
        "nomMus": nomMus,
        "nblivres": nblivres,
        "codePays": codePays,
    };
}
