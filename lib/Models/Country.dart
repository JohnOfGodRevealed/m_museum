// To parse this JSON data, do
//
//     final pays = paysFromJson(jsonString);

import 'dart:convert';

Country countryFromJson(String str) => Country.fromJson(json.decode(str));

String countryToJson(Country data) => json.encode(data.toJson());

class Country {
    Country({
        required this.codePays,
        required this.nbhabitant,
    });

    String codePays;
    int nbhabitant;

    factory Country.fromJson(Map<String, dynamic> json) => Country(
        codePays: json["codePays"],
        nbhabitant: json["nbhabitant"],
    );

    Map<String, dynamic> toJson() => {
        "codePays": codePays,
        "nbhabitant": nbhabitant,
    };
}
