// To parse this JSON data, do
//
//     final bibliotheque = bibliothequeFromJson(jsonString);

import 'dart:convert';

// Library bibliothequeFromJson(String str) => Bibliotheque.fromJson(json.decode(str));
//
// String ToJson(Library data) => json.encode(data.toJson());
//
// Library deserialize(String str) => Library.fromJson(json.decode(str));
//
// String serialize(Library data) => json.encode(data.toJson());


Library libraryFromJson(String str) => Library.fromJson(json.decode(str));

String libraryToJson(Library data) => json.encode(data.toJson());

class Library {
    Library({
        required this.numMus,
        required this.isbn,
        required this.dateAchat,
    });

    int numMus;
    String isbn;
    String dateAchat;

    factory Library.fromJson(Map<String, dynamic> json) => Library(
        numMus: json["numMus"],
        isbn: json["isbn"],
        dateAchat: json["dateAchat"],
    );

    Map<String, dynamic> toJson() => {
        "numMus": numMus,
        "isbn": isbn,
        "dateAchat": dateAchat,
    };
}
