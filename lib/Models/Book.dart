// To parse this JSON data, do
//
//     final ouvrage = ouvrageFromJson(jsonString);

import 'dart:convert';

Book bookFromJson(String str) => Book.fromJson(json.decode(str));

String bookToJson(Book data) => json.encode(data.toJson());

class Book {
  Book({
    required this.isbn,
    required this.nbPage,
    required this.titre,
    required this.codePays,
  });

  String isbn;
  int nbPage;
  String titre;
  String codePays;

  factory Book.fromJson(Map<String, dynamic> json) => Book(
        isbn: json["isbn"],
        nbPage: json["nbPage"],
        titre: json["titre"],
        codePays: json["codePays"],
      );

  Map<String, dynamic> toJson() => {
        "isbn": isbn,
        "nbPage": nbPage,
        "titre": titre,
        "codePays": codePays,
      };
}
