import '../Dao/CountryDao.dart';
import '../Models/Country.dart';

class CountriesRepository{
  final countryDao = CountryDao();

  Future<List<Country>> index() => countryDao.index();

  Future create(Country country) => countryDao.create(country);

  Future update(Country country) => countryDao.update(country);

  Future destroy(Country country) => countryDao.destroy(country);

  Future<bool> getFromOtherTables(String code) => countryDao.getCountryFromOtherTables(code);
}