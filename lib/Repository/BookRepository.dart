import 'package:m_museum/Dao/BookDao.dart';
import 'package:m_museum/Models/Book.dart';

class BookRepository {
  final bookDao = BookDao();

  Future<List<Book>> index() => bookDao.index();

  Future create(Book book) => bookDao.create(book);

  Future update(Book book) => bookDao.update(book);

  Future destroy(Book book) => bookDao.destroy(book);

  Future<bool> getFromOtherTables(String ISBN) =>
      bookDao.getBookFromOtherTables(ISBN);
}
