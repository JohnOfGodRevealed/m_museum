import 'package:m_museum/Dao/LibraryDao.dart';
import 'package:m_museum/Models/Library.dart';

class LibraryRepository {
  final libraryDao = LibraryDao();

  Future index() => libraryDao.index();

  Future create(Library library) => libraryDao.create(library);

  Future update(Library library) => libraryDao.update(library);

  Future destroy(String isbn, int numMus) => libraryDao.destroy(isbn, numMus);
}
