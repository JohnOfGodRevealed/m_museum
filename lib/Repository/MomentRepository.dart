import 'package:m_museum/Dao/MomentDao.dart';
import 'package:m_museum/Models/Moment.dart';

class MomentRepository{
  final momentDao = MomentDao();

  Future<List<Moment>> getAllMoment() => momentDao.getMoment();

}