
import 'package:m_museum/Dao/VisitDao.dart';
import 'package:m_museum/Models/Visit.dart';

class VisitRepository{
  final visitDao = VisitDao();

  Future index() => visitDao.index();

  Future create(Visit visit) => visitDao.create(visit);

  Future update(Visit visit) => visitDao.update(visit);

  Future destroy(String jour, int numMus) => visitDao.destroy(jour, numMus);
}