import 'package:m_museum/Models/Museum.dart';

import '../Dao/MuseumDao.dart';

class MuseumRepository {
  final museumDao = MuseumDao();

  Future<List<Museum>> index() => museumDao.index();

  Future create(Museum museum) => museumDao.create(museum);

  Future update(Museum museum) => museumDao.update(museum);

  Future destroy(Museum museum) => museumDao.destroy(museum);

  Future<bool> getFromOtherTables(int numMus) =>
      museumDao.getMuseumFromOtherTables(numMus);
}
